#!/usr/bin/env python
# coding: utf-8

# # Exploring Impact of Covid-19 on digital learning
# #By- Aarush Kumar
# #Dated: August 08,2021

# In[2]:


from IPython.display import Image
Image(url='https://res.cloudinary.com/people-matters/image/upload/q_auto,f_auto/v1505193006/1505193005.jpg')


# In[4]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import plotly
import plotly.express as px
import plotly.graph_objs as go
from geopy.geocoders import Nominatim
import folium
from folium.plugins import HeatMap
from folium.plugins import FastMarkerCluster
from plotly import tools
import re
from plotly.offline import init_notebook_mode, plot, iplot
from wordcloud import WordCloud, STOPWORDS 
from warnings import filterwarnings
filterwarnings('ignore')
import missingno as msno
import glob


# In[5]:


path = '/home/aarush100616/Downloads/Projects/Impact of Covid on e-learning/engagement_data' 
all_files = glob.glob(path + "/*.csv")
li = []
for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0)
    district_id = filename.split("/")[4].split(".")[0]
    df["district_id"] = district_id
    li.append(df)
engagement_df = pd.concat(li)
engagement_df = engagement_df.reset_index(drop=True)


# In[6]:


engagement_df.head(15).style.set_caption("Engagement Dataframe").set_properties(**{'background-color': 'peachpuff',
                           'color': 'midnightblue','border': '1.5px  solid black'})


# In[8]:


districts_df = pd.read_csv("/home/aarush100616/Downloads/Projects/Impact of Covid on e-learning/districts_info.csv")
districts_df.head(15).style.set_caption("Districts Dataframe").set_properties(**{'background-color': 'peachpuff',
                           'color': 'midnightblue','border': '1.5px solid black'})


# In[9]:


products_df = pd.read_csv("/home/aarush100616/Downloads/Projects/Impact of Covid on e-learning/products_info.csv")
products_df.head(15).style.set_caption("Products Dataframe").set_properties(**{'background-color': 'peachpuff',
                           'color': 'midnightblue','border': '1.5px solid black'})


# In[10]:


products_df.info()


# In[11]:


print('⁕'*50)
print("Null Value in Engagement Dataframe:")
print('⁕'*50)
print(engagement_df.isnull().sum())
print("\n")
print('⁕'*50)
print("Null Value in Districts Dataframe:")
print('⁕'*50)
print(districts_df.isnull().sum())
print("\n")
print('⁕'*50)
print("Null Value in Products Dataframe:")
print('⁕'*50)
print(products_df.isnull().sum())


# In[12]:


print("District Dataframe Columns:")
print('⁕'*50)
print(districts_df.columns)
print("\n")
print("Engagement Dataframe Columns:")
print('⁕'*50)
print(engagement_df.columns)
print("\n")
print("Products Dataframe Columns:")
print('⁕'*50)
print(products_df.columns)


# In[13]:


print('⁕'*50)
print("Shape for Districts Dataframe:",districts_df.shape)
print("Shape for Engagement Dataframe:",engagement_df.shape)
print("Shape for Products Dataframe:",products_df.shape)
print('⁕'*50)


# In[14]:


msno.bar(products_df, figsize=(10,5), fontsize=12)
plt.show()


# In[15]:


msno.bar(engagement_df, figsize=(10,5), fontsize=12)
plt.show()


# In[16]:


msno.bar(districts_df, figsize=(10,5), fontsize=12)
plt.show()


# In[17]:


print('⁕'*50)
print("Number of Duplicate value in Districts Dataframe:",districts_df.duplicated().sum())
print("Number of Duplicate value in Engagement Dataframe:",engagement_df.duplicated().sum())
print("Number of Duplicate value in Products Dataframe:",products_df.duplicated().sum())
print('⁕'*50)


# ## Geo-Analysis

# In[18]:


locations=pd.DataFrame({"Name":districts_df['state'].unique()})


# In[19]:


geolocator=Nominatim(user_agent="app")
lat=[]
lon=[]
for location in locations['Name']:
    location = geolocator.geocode(location)    
    if location is None:
        lat.append(np.nan)
        lon.append(np.nan)
    else:
        lat.append(location.latitude)
        lon.append(location.longitude)


# In[20]:


locations['lat']=lat
locations['lon']=lon


# In[21]:


Rest_locations=pd.DataFrame(districts_df['state'].value_counts().reset_index())


# In[22]:


Rest_locations.columns=['Name','count']
final_loc=Rest_locations.merge(locations,on='Name',how="left").dropna()
final_loc.head(15).style.set_caption("Locations Dataframe").set_properties(**{'background-color': 'peachpuff',
                           'color': 'midnightblue','border': '1.5px solid black'})


# In[23]:


def generateBaseMap(default_location=[37.0902, -95.7129], default_zoom_start=4):
    base_map = folium.Map(location=default_location, zoom_start=default_zoom_start)
    return base_map


# In[24]:


basemap=generateBaseMap()


# In[25]:


HeatMap(final_loc[['lat','lon','count']],zoom=20,radius=20).add_to(basemap)


# In[26]:


basemap


# In[27]:


FastMarkerCluster(data=final_loc[['lat','lon','count']].values.tolist()).add_to(basemap)
basemap


# ## Donut-Chart

# In[28]:


labels = list(districts_df.locale.value_counts().index)
values = districts_df['state'].value_counts()
# colors = ['mediumslateblue', 'darkorange']
fig = go.Figure(data=[go.Pie(labels=labels,
                             values=values,hole=.3)])
fig.update_traces(hoverinfo='label+percent', textinfo='percent', textfont_size=20,
                  marker=dict( line=dict(color='#000000', width=3)))
fig.update_layout(title="Locale Distribution ",
                  titlefont={'size': 30},      
                  )
fig.show()


# In[29]:


labels = list(districts_df.state.value_counts().index)
values = districts_df['state'].value_counts()
# colors = ['mediumslateblue', 'darkorange']
fig = go.Figure(data=[go.Pie(labels=labels,
                             values=values,hole=.3)])
fig.update_traces(hoverinfo='label+percent', textinfo='percent', textfont_size=20,
                  marker=dict( line=dict(color='#000000', width=3)))
fig.update_layout(title="State Distribution ",
                  titlefont={'size': 30},      
                  )
fig.show()


# In[30]:


labels = list(products_df['Sector(s)'].value_counts().index)
values = products_df['Sector(s)'].value_counts()
# colors = ['mediumslateblue', 'darkorange']
fig = go.Figure(data=[go.Pie(labels=labels,
                             values=values,hole=.3)])
fig.update_traces(hoverinfo='label+percent', textinfo='percent', textfont_size=20,
                  marker=dict( line=dict(color='#000000', width=3)))
fig.update_layout(title="Sector Distribution ",
                  titlefont={'size': 30},      
                  )
fig.show()


# ## Word Cloud

# In[31]:


cloud = WordCloud(width=1440, height=1080,stopwords={'nan'}).generate(" ".join(districts_df['state'].astype(str)))
plt.figure(figsize=(15, 10))
plt.imshow(cloud)
plt.axis('off')


# In[34]:


cloud = WordCloud(width=1440, height=1080,stopwords={'nan'}).generate(" ".join(products_df['Product Name'].astype(str)))
plt.figure(figsize=(15, 10))
plt.imshow(cloud)
plt.axis('off')


# In[35]:


cloud = WordCloud(width=1440, height=1080,stopwords={'nan'}).generate(" ".join(products_df['Provider/Company Name'].astype(str)))
plt.figure(figsize=(15, 10))
plt.imshow(cloud)
plt.axis('off')

